package co.com.easysolutions;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableJpaAuditing(auditorAwareRef = "customAuditorAware")
public class CalculadoraApiApplication {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(CalculadoraApiApplication.class, args);
	}
	
	/**
	 * Swagger Doc
	 * @return
	 */
	@Bean
    public Docket swagger() {
        return new Docket(DocumentationType.SWAGGER_2)
        		.groupName("CalculadoraApi")
                .select()
                .apis(RequestHandlerSelectors.basePackage("co.com.easysolutions"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getApiInfo());
    }
	
	/**
	 * Aplication Info
	 * @return
	 */
	private ApiInfo getApiInfo() {
	    return new ApiInfo(
	            "CALCULADORA REST API",
	            "Esta api provee una serie de servicios que sirven para realizar operaciones",
	            "1.0.0",
	            "Rest",
	            new Contact("Omar Guillermo Villamil", "https://gitlab.com/ovillamil89/pruebaeasysolutions", "omar.villamil.v@gmail.com"),
	            "2019 - Omar Guillermo Villamil",
	            "https://gitlab.com/ovillamil89/pruebaeasysolutions",
	            Collections.emptyList()
	    );
	}

}
