/**
 * 
 */
package co.com.easysolutions.bo;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author ovillamil
 *
 */
@Entity
@Table(name = "CALCULO")
public class CalculoBO extends AuditableEntity {

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="calculo_id_seq")
    @SequenceGenerator(name="calculo_id_seq", sequenceName="calculo_id_seq", allocationSize=1)
	private Integer id;
	
	@Column(name = "SESION")
	private String sesion;
	
	@OneToMany( cascade = CascadeType.ALL)
	private List<OperandoBO> operandos;
	
	@Column(name = "OPERADOR")
	private String operador;

	/**
	 * 
	 */
	public CalculoBO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id
	 * @param sesion
	 * @param operandos
	 * @param operador
	 */
	public CalculoBO(String sesion, List<OperandoBO> operandos, String operador) {
		super();
		this.sesion = sesion;
		this.operandos = operandos;
		this.operador = operador;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the sesion
	 */
	public String getSesion() {
		return sesion;
	}

	/**
	 * @param sesion the sesion to set
	 */
	public void setSesion(String sesion) {
		this.sesion = sesion;
	}

	/**
	 * @return the operandos
	 */
	public List<OperandoBO> getOperandos() {
		return operandos;
	}

	/**
	 * @param operandos the operandos to set
	 */
	public void setOperandos(List<OperandoBO> operandos) {
		this.operandos = operandos;
	}

	/**
	 * @return the operador
	 */
	public String getOperador() {
		return operador;
	}

	/**
	 * @param operador the operador to set
	 */
	public void setOperador(String operador) {
		this.operador = operador;
	}
	
}
