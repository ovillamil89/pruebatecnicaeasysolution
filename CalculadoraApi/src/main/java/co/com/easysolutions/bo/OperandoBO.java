/**
 * 
 */
package co.com.easysolutions.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author ovillamil
 *
 */
@Entity
@Table(name = "OPERANDO")
public class OperandoBO extends AuditableEntity {

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="operando_id_seq")
    @SequenceGenerator(name="operando_id_seq", sequenceName="operando_id_seq", allocationSize=1)
	private Integer idOperando;
	
	@Column(name = "VALOR")
	private String valor;

	/**
	 * 
	 */
	public OperandoBO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param idOperando
	 * @param valor
	 */
	public OperandoBO(Integer idOperando, String valor) {
		super();
		this.valor = valor;
	}

	/**
	 * @return the idOperando
	 */
	public Integer getIdOperando() {
		return idOperando;
	}

	/**
	 * @param idOperando the idOperando to set
	 */
	public void setIdOperando(Integer idOperando) {
		this.idOperando = idOperando;
	}

	/**
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}
}
