/**
 * 
 */
package co.com.easysolutions.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.easysolutions.dto.AuditoriaResponse;
import co.com.easysolutions.dto.CalculoRequestDTO;
import co.com.easysolutions.service.AuditoriaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author ovillamil
 *
 */
@RestController
@CrossOrigin
@RequestMapping("/api/auditoria")
@Api(description = "Controlodor para los servicios de auditoria")
public class AuditoriaController {

	@Autowired
	AuditoriaService service;
	
	private static final Logger logger = LoggerFactory.getLogger(CalculoController.class);
	
	/**
	 * Auditar transacciones  por session
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/auditarBySesion", method = RequestMethod.POST)
	@ApiOperation("Rest Service Auditar - Auditoria de las trasacciones por session.")
    public ResponseEntity<AuditoriaResponse> auditar (@RequestBody CalculoRequestDTO request) {
		logger.info("Rest Service Auditar - POST");
		AuditoriaResponse response = new AuditoriaResponse();
		
		if (request == null) {      	
        	logger.error("Error Petición No null");
        	return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        } else 
        	if (!request.getSesion().substring(0, 4).equals("Test")) {
	        	logger.error("Error Petición No Valida");
	        	return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);        	
        }
		
        return new ResponseEntity<>(service.auditar(request), HttpStatus.OK);
    }
}
