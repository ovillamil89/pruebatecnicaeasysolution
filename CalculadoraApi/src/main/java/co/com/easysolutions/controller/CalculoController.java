/**
 * 
 */
package co.com.easysolutions.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.easysolutions.dto.CalculoRequestDTO;
import co.com.easysolutions.dto.CalculoResponseDTO;
import co.com.easysolutions.service.CalculoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author ovillamil
 *
 */
@RestController
@CrossOrigin
@RequestMapping("/api/calculo")
@Api(description = "Controlodor de los servicios asociados con el proyecto")
public class CalculoController {
	
	@Autowired
	CalculoService service;
	
	private static final Logger logger = LoggerFactory.getLogger(CalculoController.class);
	
	/**
	 * Rest Service Generar Sesion
	 * @return
	 */
	@RequestMapping(value = "/generarSesion", method = RequestMethod.GET)
	@ApiOperation("Rest Service Generar Sesion - Genera un id de session donde empezar a operar.")
    public ResponseEntity<CalculoResponseDTO> generarSesion () {
		logger.info("Rest Service Generar Sesion - GET");
		CalculoResponseDTO response = service.generarSesion();
		
        if (response == null) {
        	logger.error("Error Generar Sesion");
        	return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
	
	/**
	 * Rest Service agregar operando
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/agregarOperando", method = RequestMethod.PUT)
	@ApiOperation("Rest Service agregar operando - Permite agregar valores a fin de operar sobre ellos.")
    public ResponseEntity<CalculoResponseDTO> agregarOperando(@RequestBody CalculoRequestDTO request) {
		logger.info(" - PUT");		
		CalculoResponseDTO response = new CalculoResponseDTO();

        if (request == null) {
        	response.setCodigo("400");
        	response.setRespuesta("Session no encontrada");
        	
        	logger.error("Error Petición No null");
        	return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        } else 
        	if (!request.getSesion().substring(0, 4).equals("Test")) {
	        	response.setCodigo("400");
	        	response.setRespuesta("Session no valida");
	        	
	        	logger.error("Error Petición No Valida");
	        	return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);        	
        }
        return new ResponseEntity<>(service.agregarOperandos(request), HttpStatus.OK);
    }
	
	/**
	 * Rest Service Operar
	 * @param operador
	 * @return
	 */
	@RequestMapping(value = "/operar", method = RequestMethod.POST)
	@ApiOperation("Rest Service operar - Permite realizar operaciones con los diferentes operandos ingresados.")
    public ResponseEntity<Integer> operar (@RequestBody CalculoRequestDTO request) {
		logger.info("Rest Service Operar - POST");		
		String operador = request.getOperador();
		
        if (operador.contains("SUMA") || operador.contains("RESTA") || operador.contains("MULTIPLICACION") || operador.contains("DIVISION") || operador.contains("POTENCIA")) {
        	return new ResponseEntity<>(service.operar(request), HttpStatus.OK);
        } else {
        	logger.error("Error Operador No Valido");
        	return new ResponseEntity<>(0, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
