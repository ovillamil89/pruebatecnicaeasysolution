/**
 * 
 */
package co.com.easysolutions.dto;

import java.io.Serializable;

/**
 * @author ovillamil
 *
 */
public class AuditoriaResponse implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String sesion;
	
	private String fechaCreacion;
	
	private String fechaModificacion;
	
	private String usuarioModificador;

	/**
	 * 
	 */
	public AuditoriaResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param sesion
	 * @param fechaCreacion
	 * @param fechaModificacion
	 * @param usuarioModificador
	 */
	public AuditoriaResponse(String sesion, String fechaCreacion, String fechaModificacion, String usuarioModificador) {
		super();
		this.sesion = sesion;
		this.fechaCreacion = fechaCreacion;
		this.fechaModificacion = fechaModificacion;
		this.usuarioModificador = usuarioModificador;
	}

	/**
	 * @return the sesion
	 */
	public String getSesion() {
		return sesion;
	}

	/**
	 * @param sesion the sesion to set
	 */
	public void setSesion(String sesion) {
		this.sesion = sesion;
	}

	/**
	 * @return the fechaCreacion
	 */
	public String getFechaCreacion() {
		return fechaCreacion;
	}

	/**
	 * @param fechaCreacion the fechaCreacion to set
	 */
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	/**
	 * @return the fechaModificacion
	 */
	public String getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion the fechaModificacion to set
	 */
	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	/**
	 * @return the usuarioModificador
	 */
	public String getUsuarioModificador() {
		return usuarioModificador;
	}

	/**
	 * @param usuarioModificador the usuarioModificador to set
	 */
	public void setUsuarioModificador(String usuarioModificador) {
		this.usuarioModificador = usuarioModificador;
	}

}
