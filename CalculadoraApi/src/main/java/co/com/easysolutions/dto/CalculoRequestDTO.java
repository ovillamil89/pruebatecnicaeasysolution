/**
 * 
 */
package co.com.easysolutions.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author ovillamil
 *
 */
@ApiModel(description = "Clase representativa de la petición enviada")
public class CalculoRequestDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(notes = "Campo del id de la sesion")
	private String sesion;
	
	@ApiModelProperty(notes = "Campo Lista designado para almacenar los valores operando")
	private List<Integer> operandos;
	
	@ApiModelProperty(notes = "Campo designado para operar los operandos")
	private String operador;

	/**
	 * 
	 */
	public CalculoRequestDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param sesion
	 * @param operandos
	 * @param operador
	 */
	public CalculoRequestDTO(String sesion, List<Integer> operandos, String operador) {
		super();
		this.sesion = sesion;
		this.operandos = operandos;
		this.operador = operador;
	}

	/**
	 * @return the sesion
	 */
	public String getSesion() {
		return sesion;
	}

	/**
	 * @param sesion the sesion to set
	 */
	public void setSesion(String sesion) {
		this.sesion = sesion;
	}

	/**
	 * @return the operandos
	 */
	public List<Integer> getOperandos() {
		return operandos;
	}

	/**
	 * @param operandos the operandos to set
	 */
	public void setOperandos(List<Integer> operandos) {
		this.operandos = operandos;
	}

	/**
	 * @return the operador
	 */
	public String getOperador() {
		return operador;
	}

	/**
	 * @param operador the operador to set
	 */
	public void setOperador(String operador) {
		this.operador = operador;
	}
}
