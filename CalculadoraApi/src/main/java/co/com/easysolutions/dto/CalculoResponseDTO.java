/**
 * 
 */
package co.com.easysolutions.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author ovillamil
 *
 */
@ApiModel(description = "Clase representativa de la respuesta de la petición")
public class CalculoResponseDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "Campo codigo disignado de respuesta de la trasacción")
	private String codigo;
	
	@ApiModelProperty(notes = "Campo mensaje disignado de respuesta de la trasacción")
	private String respuesta;
	
	@ApiModelProperty(notes = "Campo disignado para retornar el id de cada session")
	private String session;

	/**
	 * 
	 */
	public CalculoResponseDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param codigo
	 * @param respuesta
	 * @param resultado
	 */
	public CalculoResponseDTO(String codigo, String respuesta, String session) {
		super();
		this.codigo = codigo;
		this.respuesta = respuesta;
		this.session = session;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the respuesta
	 */
	public String getRespuesta() {
		return respuesta;
	}

	/**
	 * @param respuesta the respuesta to set
	 */
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	/**
	 * @return the resultado
	 */
	public String getSession() {
		return session;
	}

	/**
	 * @param resultado the resultado to set
	 */
	public void setSession(String session) {
		this.session = session;
	}
	
}
