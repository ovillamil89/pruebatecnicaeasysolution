/**
 * 
 */
package co.com.easysolutions.implementation;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import co.com.easysolutions.dto.AuditoriaResponse;
import co.com.easysolutions.service.CalculoService;

/**
 * @author ovillamil
 *
 */
@Service
public class AuditoriaImplementacion {

	@PersistenceUnit
    private EntityManagerFactory emf;
	
	private static final Logger logger = LoggerFactory.getLogger(CalculoService.class);
	
	/**
	 * Consultar Auditoria
	 * @param sesion
	 * @return
	 */
	public AuditoriaResponse consultarAuditoria (String sesion) {
		logger.info("calcularResultado - Implementación de servicio");
		AuditoriaResponse auditoria = new AuditoriaResponse();
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		try {
			Query query =  em.createNativeQuery("select sesion\r\n" + 
					"from public.calculo where sesion = '" + sesion + "';");
			auditoria.setSesion((String) query.getSingleResult());
			query =  em.createNativeQuery("select created_date\r\n" + 
					"from public.calculo where sesion = '" + sesion + "';");
			auditoria.setFechaCreacion(query.getSingleResult().toString());
			query =  em.createNativeQuery("select last_modified_date\r\n" + 
					"from public.calculo where sesion = '" + sesion + "';");
			auditoria.setFechaModificacion(query.getSingleResult().toString());
			query =  em.createNativeQuery("select last_modified_by\r\n" + 
					"from public.calculo where sesion = '" + sesion + "';");
			auditoria.setUsuarioModificador((String) query.getSingleResult());
			
		} catch (NoResultException e) {
			// TODO: handle exception
			logger.error(e.getMessage());
			e.printStackTrace();
		}	
		em.getTransaction().commit();
		em.close();

		return auditoria;
	}
}
