/**
 * 
 */
package co.com.easysolutions.implementation;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import co.com.easysolutions.bo.CalculoBO;
import co.com.easysolutions.bo.OperandoBO;
import co.com.easysolutions.dto.CalculoRequestDTO;
import co.com.easysolutions.service.CalculoService;

/**
 * @author ovillamil
 *
 */
@Service
public class CalculoImplementation {
	
	@PersistenceUnit
    private EntityManagerFactory emf;
	
	private static final Logger logger = LoggerFactory.getLogger(CalculoService.class);

	/**
	 * Tranaform data dto to bo
	 * @param dto
	 * @return
	 */
	public CalculoBO dtoToBo (CalculoRequestDTO dto) {
		logger.info("dtoToBo - Implementación de servicio");
		CalculoBO calculoBO = new CalculoBO();
		List<OperandoBO> operandosBO = new ArrayList<OperandoBO>();
		
		try {
			if (dto != null) {
				calculoBO.setSesion(dto.getSesion());
				if (!dto.getOperandos().isEmpty()) {
					for (Integer element : dto.getOperandos()) {
						OperandoBO operandoBO = new OperandoBO();
						operandoBO.setValor(element.toString());
						operandosBO.add(operandoBO);
					}
				}
				calculoBO.setOperandos(operandosBO);
				if (dto.getOperador() != null)
					calculoBO.setOperador(dto.getOperador().substring(0, 1));
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}

		return calculoBO;
	}
	
	/** 
	 * Generate new Sesion
	 * @return
	 */
	public String generateSesion () {	
		logger.info("generateSesion - Implementación de servicio");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		String sesionStr = "";
		BigInteger consecutive = null;
		
		try {
			Query query =  em.createNativeQuery("select max(substring(sesion from length(sesion) for length(sesion)))\r\n" + 
					"from public.calculo");
			sesionStr = (String) query.getSingleResult();
			
			if(sesionStr == null || sesionStr.equals(""))
				consecutive = new BigInteger("0");	
			else
				consecutive = new BigInteger(sesionStr);
		} catch (NoResultException e) {
			// TODO: handle exception
			consecutive = new BigInteger("0");
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		consecutive = consecutive.add(new BigInteger("1"));		
		em.getTransaction().commit();
		em.close();

		return "Test-" + String.format("%03d", consecutive);
	}
	
	/**
	 * Get all value to operando
	 * @param operation
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Integer> calcularResultado (String sesion) {
		logger.info("calcularResultado - Implementación de servicio");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		List<String> resultadoStr = new ArrayList<String>();
		List<Integer> resultadoInt = new ArrayList<Integer>();
		
		try {
			Query query =  em.createNativeQuery("select op.valor \r\n" + 
					"from public.operando op, public.calculo ca, public.calculo_operandos co \r\n" + 
					"where ca.id = co.calculobo_id and\r\n" + 
					"op.id_operando = co.operandos_id_operando and\r\n" + 
					"ca.sesion = '" + sesion + "';");
			resultadoStr = query.getResultList();
							
			if(resultadoStr == null || resultadoStr.isEmpty())
				resultadoInt.add(0);
			else {
				for (String element : resultadoStr) {
					if (element != null && element != "")
						resultadoInt.add(Integer.parseInt(element));
				}
			}
		} catch (NoResultException e) {
			// TODO: handle exception
			resultadoInt.add(0);
			logger.error(e.getMessage());
			e.printStackTrace();
		}	
		em.getTransaction().commit();
		em.close();

		return resultadoInt;
	}
	
	/**
	 * Get result to operation
	 * @param valores
	 * @param operation
	 * @return
	 */
	public Integer calcularArreglo (List<Integer> valores, String operation) {
		logger.info("calcularArreglo - Implementación de servicio");
		Integer resultado = 0;
		if (operation.equals("SUMA")) {
			for (int i = 0; i < valores.size(); i++) {
				resultado = resultado + valores.get(i);
			}
		}
		if (operation.equals("RESTA")) {
			for (int i = 0; i < valores.size(); i++) {
				resultado = valores.get(i) - resultado;
			}
		}	
		if (operation.equals("MULTIPLICACION")) {
			resultado = 1;
			for (int i = 0; i < valores.size(); i++) {
				resultado = resultado + valores.get(i);
			}
		}	
		if (operation.equals("DIVISION")) {
			resultado = 1;
			for (int i = 0; i < valores.size(); i++) {
				resultado = valores.get(i) / resultado;
			}
		}	
		if (operation.equals("POTENCIA")) {
			resultado = 1;
			for (int i = 0; i < valores.size(); i++) {
				resultado = (int) Math.pow(valores.get(i), resultado);
			}
		}
		return resultado;
	}
	
}
