/**
 * 
 */
package co.com.easysolutions.repository;

import org.springframework.data.repository.CrudRepository;

import co.com.easysolutions.bo.CalculoBO;

/**
 * @author ovillamil
 *
 */
public interface CalculoRepository extends CrudRepository<CalculoBO, Integer> {

}
