/**
 * 
 */
package co.com.easysolutions.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import co.com.easysolutions.dto.AuditoriaResponse;
import co.com.easysolutions.dto.CalculoRequestDTO;
import co.com.easysolutions.implementation.AuditoriaImplementacion;

/**
 * @author ovillamil
 *
 */
@Service
public class AuditoriaService {
	
	@Autowired
	AuditoriaImplementacion implementacion;
	
	private static final Logger logger = LoggerFactory.getLogger(CalculoService.class);
	
	/**
	 * Auditar por sesion
	 * @param calculoDTO
	 * @return
	 */
	public AuditoriaResponse auditar (CalculoRequestDTO calculoDTO) {
		Gson gson = new Gson();
		logger.info("Auditar - Operación de servicio: " + gson.toJson(calculoDTO));
		AuditoriaResponse response = new AuditoriaResponse();
		
		try {
			response = implementacion.consultarAuditoria(calculoDTO.getSesion());
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		
		logger.info("Auditar: " + gson.toJson(response));
		return response;
	}
}
