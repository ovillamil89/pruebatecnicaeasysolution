/**
 * 
 */
package co.com.easysolutions.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import co.com.easysolutions.bo.CalculoBO;
import co.com.easysolutions.dto.CalculoRequestDTO;
import co.com.easysolutions.dto.CalculoResponseDTO;
import co.com.easysolutions.implementation.CalculoImplementation;
import co.com.easysolutions.repository.CalculoRepository;

/**
 * @author ovillamil
 *
 */
@Service
public class CalculoService {

	@Autowired
	CalculoRepository calculoRepository;
	
	@Autowired
	CalculoImplementation implementation;
	
	private static final Logger logger = LoggerFactory.getLogger(CalculoService.class);
	
	/**
	 * Generar Nueva Session
	 * @return
	 */
	public CalculoResponseDTO generarSesion () {
		Gson gson = new Gson();
		logger.info("Generar Session - Operación de servicio");
		CalculoResponseDTO calculoResponseDTO = new CalculoResponseDTO();
		
		try {
			calculoResponseDTO.setCodigo("201");
			calculoResponseDTO.setRespuesta("Sesion create");
			calculoResponseDTO.setSession(implementation.generateSesion());
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}

		logger.info("Generar Session: " + gson.toJson(calculoResponseDTO));
		return calculoResponseDTO;
	}
	
	/**
	 * Agregar operandos
	 * @param calculoDTO
	 * @return
	 */
	public CalculoResponseDTO agregarOperandos (CalculoRequestDTO calculoDTO) {
		Gson gson = new Gson();
		logger.info("Agregar Operandos - Operación de servicio: " + gson.toJson(calculoDTO));
		CalculoResponseDTO calculoResponseDTO = new CalculoResponseDTO();
		
		try {
			CalculoBO calculoBO = new CalculoBO();
			// Dto to Bo
			calculoBO = calculoRepository.save(implementation.dtoToBo(calculoDTO));
			if (calculoBO == null) {
				calculoResponseDTO.setCodigo("100");
				calculoResponseDTO.setRespuesta("Entity not save");
			} else {
				calculoResponseDTO.setCodigo("201");
				calculoResponseDTO.setRespuesta("Entity create");
				calculoResponseDTO.setSession(calculoDTO.getSesion());
			}			
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		
		logger.info("Agregar Operandos: " + gson.toJson(calculoResponseDTO));
		return calculoResponseDTO;
	}
	
	/**
	 * Operar
	 * @param calculoDTO
	 * @return
	 */
	public Integer operar (CalculoRequestDTO calculoDTO) {
		Gson gson = new Gson();
		logger.info("Operar - Operación de servicio: " + gson.toJson(calculoDTO));
		Integer resultado = null;
		
		try {
			List<Integer> valores = implementation.calcularResultado(calculoDTO.getSesion());
			try {
				resultado = implementation.calcularArreglo(valores, calculoDTO.getOperador());
				
				List<Integer> newOperandos = new ArrayList<Integer>();
				newOperandos.add(resultado);
				
				calculoDTO.setOperandos(newOperandos);
				calculoRepository.save(implementation.dtoToBo(calculoDTO));
			} catch (Exception e) {
				logger.error(e.getMessage());
				e.printStackTrace();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		
		logger.info("Agregar Operandos: " + gson.toJson(resultado));
		return resultado;
	}
	
}
