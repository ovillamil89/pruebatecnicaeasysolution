# Prueba Técnica EasySolutions
Prueba conjunto de servicios para calcular: suma, resta, multiplicación, división y
potenciación de un conjunto de números.
El conjunto de herramientas de la solución está diseñado por default para correr sobre Windows x64.

## Comenzando
1. Obtener una copia del proyecto en funcionamiento
git clone https://gitlab.com/ovillamil89/pruebaeasysolutions.git

Mira **Deployment** para conocer como desplegar el proyecto.

### Pre-requisitos 📋
1. Postgresql 11.4.3
2. Postman-win64-7.2.2   
3. Maven

### Instalación 🔧
1. Instalar la versión actual de postgresql https://www.postgresql.org/download/windows/
   postgresql-11.4-3-windows-x64

2. Verificar que user y el password de la base de datos sea postgres
   user: postgres
   password: postgres
   
3. Validar que la base de datos por defecto postgres con el esquema public este crearla, si no se encuentra creada ir a paso 4 en caso contrario omitir paso 4.

4. Crear base de datos postgres
	CREATE DATABASE postgres
		WITH 
		OWNER = postgres
		ENCODING = 'UTF8'
		LC_COLLATE = 'Spanish_Colombia.1252'
		LC_CTYPE = 'Spanish_Colombia.1252'
		TABLESPACE = pg_default
		CONNECTION LIMIT = -1;

	COMMENT ON DATABASE postgres IS 'default administrative connection database';

5. Instalar un cliente para consumir servicios Rest https://www.getpostman.com/downloads/
   Postman-win64-7.2.2-Setup
   
6. Importar la colección de prueba "TestEasySolution.postman_collection" del forder binarios.
   
3. Instalar Maven en Windows
	-Descargar maven https://maven.apache.org/download.cgi
	-Agregar carpeta \bin al path
	-Crea una variable de entorno M2_HOME con la ruta de maven
	-Verificar mvn -v

## Ejecutando las pruebas ⚙️
1.	Dirigirse al directorio “CalculadoraApi\target” ejecutar el jar “CalculadoraApi-0.0.1” (Para el caso de no tener el archivo .jar mira **Deployment** para conocer como desplegar el proyecto.
Java -jar CalculadoraApi-0.0.1

1.	Para verificar la documentación del proyecto dirigirse a la siguiente URL
http://localhost:8090/swagger-ui.html#/

2.	En la colección postman "TestEasySolution.postman_collection" encontrara los tres servicios correspondientes a la solución.

## Deployment 📦

1.	Obtener una copia del proyecto en funcionamiento
git clone https://gitlab.com/ovillamil89/pruebatecnicaeasysolution.git

2.	En el directorio “CalculadoraApi” ejecutar los comandos.
mvn clean
mvn install

## Autor ✒️

Omar Guillermo Villamil Velasco
Omar.villamil.v@gmail.com